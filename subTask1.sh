#!/bin/bash

SPLITTER=';'

for ARGUMENT in "$@"
do
KEY=$(echo $ARGUMENT | cut -f1 -d=)
VALUE=$(echo $ARGUMENT | cut -f2 -d=)   

case "$KEY" in
INPUT)      INPUT=${VALUE} ;; 
OUTPUT)     OUTPUT=${VALUE} ;; 
WORKERS)    WORKERS=${VALUE} ;;
LINK_COL)   LINK_COL=${VALUE} ;;
esac    
done


LINK_COL_IDX=$(head -n 1 $INPUT | tr -s "$SPLITTER" '\n' | nl -nln | grep -w "$LINK_COL" | cut -f1)
echo $LINK_COL_IDX
LINKS=($(cut -d "$SPLITTER" -f $LINK_COL_IDX $INPUT))


mkdir -p "$OUTPUT" 
cd "$OUTPUT" 
echo "${LINKS[@]:1:10000}" | xargs -P "$WORKERS" -n 1 curl --create-dirs -O


# ./subTask1.sh INPUT=file.csv OUTPUT=save_path WORKERS=8 LINK_COL=link 
